terraform {
  backend "s3" {
    bucket = "gitops-snt"
    key    = "eks-deployment/terraform.tfstate"
    region = "ap-southeast-1"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.10.0"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.10.1"
    }
  }
}

provider "aws" {
    region = "ap-southeast-1"
}
